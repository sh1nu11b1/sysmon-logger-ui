package main

import (
    util "github.com/woanware/goutil"
)

//
func processIntParameter(data string) (int, bool) {
    if len(data) > 0 {
        if util.IsNumber(data) == true {
            return util.ConvertStringToInt(data), true
        }
    }

    return -1, false
}

//
func processInt64Parameter(data string) (int64, bool) {
    if len(data) > 0 {
        if util.IsNumber(data) == true {
            return util.ConvertStringToInt64(data), true
        }
    }

    return -1, false
}

//
func processCurrentPageNumber(data string, mode string) (int) {
    if len(data) == 0 {
        return 0
    }

    if util.IsNumber(data) == false {
        return 0
    }

    currentPageNumber := util.ConvertStringToInt(data)

    if mode == "first"{
        return 0
    }

    if mode == "next" {
        currentPageNumber += 1
        return currentPageNumber
    }

    if mode == "previous" {
        currentPageNumber -= 1
        return currentPageNumber
    }

    if currentPageNumber < 0 {
        return 0
    }

    return 0
}
//
//func getHuntQuery(id int) (dataType int, sql string) {
//    for _, v := range huntQueries {
//        if v.Id == id {
//            return v.DataType, v.QuerySql
//        }
//    }
//
//    return 0, ""
//}
