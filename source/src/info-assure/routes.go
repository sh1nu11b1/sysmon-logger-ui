package main

import (
	util "github.com/woanware/goutil"
	"github.com/gin-gonic/gin"
	"net/http"
	"html/template"
    "path"
    "strings"
)

// ##### Methods #############################################################

//
func routeIndex (c *gin.Context) {
	c.HTML(http.StatusOK, "index", gin.H{
	})
}

//
func routeEvents (c *gin.Context) {
	currentPageNumber := 0
	numRecsPerPage := 10

	temp :=  c.PostForm("num_recs_per_page")
	if len(temp) > 0 {
		if util.IsNumber(temp) == true {
			numRecsPerPage = util.ConvertStringToInt(temp)
		}
	}

	mode, hasMode := c.GetPostForm("mode")

	// Appears to be the first request to send the initial set of data
	if (mode != "first" &&
		mode != "next" &&
		mode != "previous") || hasMode == false {

		loadEventData(c, currentPageNumber, numRecsPerPage)
		return
	}

	temp =  c.PostForm("current_page_num")
	if len(temp) == 0 {
		currentPageNumber = 0
	}

	if util.IsNumber(temp) == false {
		currentPageNumber = 0
	}

	currentPageNumber = util.ConvertStringToInt(temp)

	if mode == "first"{
		currentPageNumber = 0
	}

	if mode == "next" {
		currentPageNumber += 1
	}

	if mode == "previous" {
		currentPageNumber -= 1
	}

	if currentPageNumber < 0 {
		currentPageNumber = 0
	}

	loadEventData(c, currentPageNumber, numRecsPerPage)
}

//
func loadEventData(c *gin.Context, currentPageNumber int, numRecsPerPage int) {

	errored, noMoreRecords, events := getEvents(numRecsPerPage, currentPageNumber)
	if errored == true {
		c.String(http.StatusInternalServerError, "")
		return
	}

	c.HTML(http.StatusOK, "events", gin.H{
		"current_page_num": currentPageNumber,
		"num_recs_per_page": numRecsPerPage,
		"no_more_records": noMoreRecords,
		"events": events,
	})
}

func getEvents(numRecsPerPage int, currentPageNumber int) (bool, bool, []*Event) {

	var data []*Event

	err := db.
		Select("id, domain, host, utc_time, type, html").
		From("event").
		OrderBy("utc_time DESC").
		Offset(uint64(numRecsPerPage * currentPageNumber)).
		Limit(uint64(numRecsPerPage + 1)).
		QueryStructs(&data)

	if err != nil {
		logger.Error(err)
	}

	// Perform some cleaning of the data, so that it displays better in the HTML
	for _, v := range data {
		v.Data = template.HTML(v.Html)
		v.UtcTimeStr = v.UtcTime.Format("15:04:05 02/01/2006")
	}

	noMoreRecords := false
	if len(data) < numRecsPerPage + 1 {
		noMoreRecords = true
	} else {
		// Remove the last item in the slice/array
		data = data[:len(data) - 1]
	}

	return false, noMoreRecords, data
}

//
func routeExport (c *gin.Context) {

    exportType := 0

    temp :=  c.PostForm("export_type")
    if len(temp) > 0 {
        if util.IsNumber(temp) == true {
            exportType = util.ConvertStringToInt(temp)
        }
    }

    if exportType == 0 {
        c.HTML(http.StatusOK, "export", gin.H{
            "has_data": false,
            "export_type": 0,
            "data": nil,
        })
        return
    }

    errored, data := getExports(exportType)
    if errored == true {
        c.String(http.StatusInternalServerError, "")
        return
    }

    hasData := true
    if len(data) == 0 {
        hasData = false
    }

    c.HTML(http.StatusOK, "export", gin.H{
        "has_data": hasData,
        "export_type": exportType,
        "data": data,
    })
}

//
func getExports(exportType int) (bool, []*Export) {

    var data []*Export

    err := db.
        Select(`*`).
        From("export").
        Where("data_type = $1", exportType).
        Limit(10).
        OrderBy("updated").
        QueryStructs(&data)

    if err != nil {
        logger.Errorf("Error querying for exports: %v (%d)", err, exportType)
        return true, data
    }

    // Perform some cleaning of the data, so that it displays better in the HTML
    for _, v := range data {
        v.OtherData = template.HTML(`<a href="/export/`+ util.ConvertInt64ToString(v.Id) + `">` + v.Updated.Format("15:04:05 02/01/2006") + `</a>`)
    }

    return false, data
}

//
func routeExportData(c *gin.Context) {

    id, successful := processInt64Parameter(c.Param("id"))
    if successful == false {
        c.String(http.StatusInternalServerError, "")
        return
    }

    if id < 1 {
        c.String(http.StatusInternalServerError, "")
        return
    }

    errored, export := getExport(id)

    if errored == true {
        c.String(http.StatusInternalServerError, "")
        return
    }

    // Load file contents
    if util.DoesFileExist(path.Join(config.ExportDir, export.FileName)) == false {
        logger.Errorf("Export file does not exist: %s", export.FileName)
        c.String(http.StatusInternalServerError, "")
        return
    }

    // Return file contents as download
    data, err := util.ReadTextFromFile(path.Join(config.ExportDir, export.FileName))
    if err != nil {
        logger.Errorf("Error reading export file: %v (%s)", err, export.FileName)
        c.String(http.StatusInternalServerError, "")
        return
    }

    c.Header("Content-Disposition", "attachment; filename=\"" + export.FileName)
    c.Data(http.StatusOK, "text/csv", []byte(data))
}

//
func getExport(id int64) (bool, Export) {

    var e Export

    err := db.
        Select(`id, data_type, file_name, updated`).
        From("export").
        Where("id = $1", id).
        QueryStruct(&e)

    if err != nil {
        logger.Errorf("Error querying for export: %v", err)
        return true, e
    }

    return false, e
}

//
func routeSearch (c *gin.Context) {

    currentPageNumber := 0

    numRecsPerPage, successful := processIntParameter(c.PostForm("num_recs_per_page"))
    if successful == false {
        numRecsPerPage = 10
    }

    mode, hasMode := c.GetPostForm("mode")

    // Appears to be the first request to send the initial set of data
    if (mode != "first" &&
        mode != "next" &&
        mode != "previous") || hasMode == false {

        loadSearchData(c, "", currentPageNumber, numRecsPerPage)
        return
    }

    searchValue :=  c.PostForm("search_value")
    if len(searchValue) == 0 {
        c.String(http.StatusInternalServerError, "")
        return
    }

    currentPageNumber = processCurrentPageNumber(c.PostForm("current_page_num"), mode)

    loadSearchData(c,  searchValue, currentPageNumber, numRecsPerPage)
}

//
func loadSearchData(
    c *gin.Context,
    searchValue string,
    currentPageNumber int,
    numRecsPerPage int) {

    if len(searchValue) == 0 {
        c.HTML(http.StatusOK, "search", gin.H{
            "current_page_num": currentPageNumber,
            "num_recs_per_page": numRecsPerPage,
            "no_more_records": true,
            "events": nil,
            "has_data": false,
            "search_value": searchValue,
        })
        return
    }

    errored, noMoreRecords, events := getSearch(searchValue, numRecsPerPage, currentPageNumber)
    if errored == true {
        c.String(http.StatusInternalServerError, "")
        return
    }

    hasData := true
    if len(events) == 0 {
        hasData = false
    }

    c.HTML(http.StatusOK, "search", gin.H{
        "current_page_num": currentPageNumber,
        "num_recs_per_page": numRecsPerPage,
        "no_more_records": noMoreRecords,
        "events": events,
        "has_data": hasData,
        "search_value": searchValue,
    })
}

//
func getSearch(searchValue string, numRecsPerPage int, currentPageNumber int) (bool, bool, []*Event) {

    var data []*Event

    err := db.
        Select(`id, domain, host, utc_time, type, html`).
        From("event").
        OrderBy("utc_time DESC").
        Offset(uint64(numRecsPerPage * currentPageNumber)).
        Limit(uint64(numRecsPerPage + 1)).
        Where("LOWER(message) LIKE $1", "%" + strings.ToLower(searchValue) + "%").
        QueryStructs(&data)

    if err != nil {
        logger.Errorf("Error querying for search: %v", err)
        return true, false, data
    }

    // Perform some cleaning of the data, so that it displays better in the HTML
    for _, v := range data {
        v.Data = template.HTML(v.Html)
        v.UtcTimeStr = v.UtcTime.Format("15:04:05 02/01/2006")
    }

    noMoreRecords := false
    if len(data) < numRecsPerPage + 1 {
        noMoreRecords = true
    } else {
        // Remove the last item in the slice/array
        data = data[:len(data) - 1]
    }

    return false, noMoreRecords, data
}

//
func routeHunts (c *gin.Context) {

	action := c.Param("action")
	logger.Infof("action %v", action)

	switch action {
	case "run":
		getHuntRun(c)
	case "add":
		getHuntAdd(c)
	case "modify":
		getHuntModify(c)
	default:
		c.Redirect(http.StatusMovedPermanently, "/hunts/run")
	}
}

//
func getHuntRun(c *gin.Context) {

	currentPageNumber := 0
	numRecsPerPage := 10

	temp :=  c.PostForm("num_recs_per_page")
	if len(temp) > 0 {
		if util.IsNumber(temp) == true {
			numRecsPerPage = util.ConvertStringToInt(temp)
		}
	}

	mode, hasMode := c.GetPostForm("mode")

	// Appears to be the first request to send the initial set of data
	if (mode != "first" &&
		mode != "next" &&
		mode != "previous") || hasMode == false {

		loadHunts(c,0,currentPageNumber, numRecsPerPage)
		return
	}

	temp =  c.PostForm("current_page_num")
	if len(temp) == 0 {
		currentPageNumber = 0
	}

	if util.IsNumber(temp) == false {
		currentPageNumber = 0
	}

	currentPageNumber = util.ConvertStringToInt(temp)

	if mode == "first"{
		currentPageNumber = 0
	}

	if mode == "next" {
		currentPageNumber += 1
	}

	if mode == "previous" {
		currentPageNumber -= 1
	}

	if currentPageNumber < 0 {
		currentPageNumber = 0
	}

	huntId, successful := processIntParameter(c.PostForm("hunt"))
	if successful == false {
		loadHunts(c,0, currentPageNumber, numRecsPerPage)
		return
	}

	if huntId == 0 {
		loadHunts(c,0, currentPageNumber, numRecsPerPage)
		return
	}

	loadHunts(c, huntId, currentPageNumber, numRecsPerPage)
}

//
func loadHunts(c *gin.Context, huntId int, currentPageNumber int, numRecsPerPage int) {

	if huntId == 0 {
		c.HTML(http.StatusOK, "hunt_run", gin.H{
			"current_page_num": currentPageNumber,
			"num_recs_per_page": numRecsPerPage,
			"no_more_records": true,
			"has_data": false,
			"data_type": "",
			"message": "",
			"hunts": getHuntQueries(),
		})

		return
	}

	hunt := getHuntQuery(huntId)
	if hunt.DataType == 0 {
		c.HTML(http.StatusOK, "hunt_run", gin.H{
			"current_page_num": currentPageNumber,
			"num_recs_per_page": numRecsPerPage,
			"no_more_records": true,
			"has_data": false,
			"data_type": "",
			"message": "",
			"hunts": getHuntQueries(),
		})
	}

	switch hunt.DataType {
	case TYPE_PROCESS_CREATE:
		errored, noMoreRecords, data := getHuntProcessCreate(hunt.QuerySql, numRecsPerPage, currentPageNumber)
		if errored == false {
			c.HTML(http.StatusOK, "hunt_run", gin.H{
				"current_page_num": currentPageNumber,
				"num_recs_per_page": numRecsPerPage,
				"no_more_records": noMoreRecords,
				"has_data": true,
				"data": data,
				"data_type": "process_create",
				"message": "",
				"hunts": getHuntQueries(),
			})
			return
		}

	case TYPE_NETWORK_CONNECTION:
		errored, noMoreRecords, data := getHuntNetworkConnection(hunt.QuerySql, numRecsPerPage, currentPageNumber)
		if errored == false {
			c.HTML(http.StatusOK, "hunt_run", gin.H{
				"current_page_num": currentPageNumber,
				"num_recs_per_page": numRecsPerPage,
				"no_more_records": noMoreRecords,
				"has_data": true,
				"data": data,
				"data_type": "network_connection",
				"message": "",
				"hunts": getHuntQueries(),
			})
			return
		}
	}

	// If we have got this far then we need to display the error message
	c.HTML(http.StatusOK, "hunt_run", gin.H{
		"current_page_num": currentPageNumber,
		"num_recs_per_page": numRecsPerPage,
		"no_more_records": true,
		"has_data": false,
		"data_type": "",
		"message": "Error running hunt",
		"hunts": getHuntQueries(),
	})
}

func getHuntAdd(c *gin.Context) {

	name, desc, sql, dataType, isValid := getHuntParameterData(c)

	if isValid == false {
		c.HTML(http.StatusOK, "hunt_add", gin.H{
			"name": name,
			"description": desc,
			"type": dataType,
			"where": sql,
			"error_message": "",
		})
		return
	}

	// Add a hunt_query record
	hq := new(HuntQuery)
	err := db.
		InsertInto("hunt_query").
		Columns("name", "description", "query_sql", "data_type").
		Values(name, desc, sql, dataType).
		QueryStruct(&hq)

	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value") == true {
			c.HTML(http.StatusOK, "hunt_add", gin.H{
				"name": name,
				"description": desc,
				"type": dataType,
				"where": sql,
				"error_message": "Hunt with same name already exists",
			})

		} else if strings.Contains(err.Error(), "no rows in result set") == false {
			logger.Errorf("Error inserting new hunt: %v", err)

			c.HTML(http.StatusOK, "hunt_add", gin.H{
				"name": name,
				"description": desc,
				"type": dataType,
				"where": sql,
				"error_message": "Error adding new hunt",
			})
		}

		return
	}

	// Reload the hunt query slice
	loadHuntQueries()

	// As we have successfully added a new hunt, now redirect to the /hunt/run page
	c.Redirect(http.StatusMovedPermanently, "/hunts/run")
}

//
func getHuntProcessCreate(sql string, numRecsPerPage int, currentPageNumber int) (bool, bool, []*ProcessCreate) {

	var data []*ProcessCreate

	err := db.
		Select(`*`).
		From("process_create").
		OrderBy("utc_time DESC").
		Offset(uint64(numRecsPerPage * currentPageNumber)).
		Limit(uint64(numRecsPerPage + 1)).
		Where(sql).
		QueryStructs(&data)

	if err != nil {
		logger.Errorf("Error querying for Process Create hunt: %v", err)
		return true, false, data
	}

	// Perform some cleaning of the data, so that it displays better in the HTML
	for _, v := range data {
		v.HtmlStr = template.HTML(v.Html)
		v.UtcTimeStr = v.UtcTime.Format("15:04:05 02/01/2006")
	}

	noMoreRecords := false
	if len(data) < numRecsPerPage + 1 {
		noMoreRecords = true
	} else {
		// Remove the last item in the slice/array
		data = data[:len(data) - 1]
	}

	return false, noMoreRecords, data
}

//
func getHuntNetworkConnection(sql string, numRecsPerPage int, currentPageNumber int) (bool, bool, []*NetworkConnection) {

	var data []*NetworkConnection

	err := db.
	Select(`*`).
		From("network_connection").
		OrderBy("utc_time DESC").
		Offset(uint64(numRecsPerPage * currentPageNumber)).
		Limit(uint64(numRecsPerPage + 1)).
		Where(sql).
		QueryStructs(&data)

	if err != nil {
		logger.Errorf("Error querying for Network Connection hunt: %v", err)
		return true, false, data
	}

	// Perform some cleaning of the data, so that it displays better in the HTML
	for _, v := range data {
		v.HtmlStr = template.HTML(v.Html)
		v.UtcTimeStr = v.UtcTime.Format("15:04:05 02/01/2006")
	}

	noMoreRecords := false
	if len(data) < numRecsPerPage + 1 {
		noMoreRecords = true
	} else {
		// Remove the last item in the slice/array
		data = data[:len(data) - 1]
	}

	return false, noMoreRecords, data
}

//
func getHuntModify(c *gin.Context) {

	c.HTML(http.StatusOK, "hunt_modify", gin.H{
		"hunts": getHuntQueries(),
	})
}

// Route used for modifying/deleting a hunt query
func routeGetModifyHunt (c *gin.Context) {

	action := c.Param("action")

	if action != "modify" {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	id, successful := processIntParameter(c.Param("id"))
	if successful == false {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	hunt := getHuntQuery(id)
	if hunt.Id == 0 {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	c.HTML(http.StatusOK, "hunt_details", gin.H{
		"id": hunt.Id,
		"name": hunt.Name,
		"description": hunt.Description,
		"type": hunt.DataType,
		"where": hunt.QuerySql,
		"message": "",
	})
}

// Route used for modifying/deleting a hunt query
func routePostModifyHunt(c *gin.Context) {

	action := c.Param("action")

	if action != "modify" {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	id, successful := processIntParameter(c.Param("id"))
	if successful == false {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	name, desc, sql, dataType, isValid := getHuntParameterData(c)

	mode := c.PostForm("mode")
	if mode == "delete" {

		// Need to perform SQL delete against the DB
		_, err := db.
			DeleteFrom("hunt_query").
			Where("id = $1", id).
			Exec()

		if err != nil {
			logger.Errorf("Error deleting hunt: %v", err)

			c.HTML(http.StatusOK, "hunt_details", gin.H{
				"id": id,
				"name": name,
				"description": desc,
				"type": dataType,
				"where": sql,
				"message": "Error deleting hunt",
			})

			return
		}

		// Reload the hunt query slice using the mutex
		loadHuntQueries()

		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	// If we have got this far, then the "mode" was not delete, so
	// it must be "update" else someone or something has gone wrong
	if mode != "update" {
		c.HTML(http.StatusOK, "hunt_modify", gin.H{
			"hunts": getHuntQueries(),
		})
		return
	}

	// At this point we will validate the various hunt details to check if they are valid.
	// The details were extracted from the request earlier, but were only needed earlier
	// for passing back to a failed delete request
	if isValid == false {
		c.HTML(http.StatusOK, "hunt_details", gin.H{
			"id": id,
			"name": name,
			"description": desc,
			"type": dataType,
			"where": sql,
			"message": "",
		})
		return
	}

	_, err := db.
		Update("hunt_query").
		Set("name", name).
		Set("description", desc).
		Set("query_sql", sql).
		Set("data_type", dataType).
		Where("id = $1", id).
		Exec()

	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value") == true {
			c.HTML(http.StatusOK, "hunt_details", gin.H{
				"id": id,
				"name": name,
				"description": desc,
				"type": dataType,
				"where": sql,
				"message": "Hunt with same name already exists",
			})

		} else if strings.Contains(err.Error(), "no rows in result set") == false {
			logger.Errorf("Error updating hunt: %v", err)

			c.HTML(http.StatusOK, "hunt_details", gin.H{
				"id": id,
				"name": name,
				"description": desc,
				"type": dataType,
				"where": sql,
				"message": "Error updating hunt",
			})
		}

		return
	}

	loadHuntQueries()

	c.HTML(http.StatusOK, "hunt_modify", gin.H{
		"hunts": getHuntQueries(),
	})
}

//
func getHuntParameterData(c *gin.Context) (name string, desc string, sql string, dataType int, valid bool) {

	name = c.PostForm("name")
	desc = c.PostForm("description")
	sql = c.PostForm("where")

	isValid := true
	if len(name) == 0 {
		isValid = false
	}

	if len(desc) == 0 {
		isValid = false
	}

	if len(sql) == 0 {
		isValid = false
	}

	dataType, successful := processIntParameter(c.PostForm("type"))
	if successful == false {
		isValid = false
	}

	valid = isValid

	return name, desc, sql, dataType, valid
}


